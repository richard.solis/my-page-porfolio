
  <div class="container blanco" id="footer">
<div class="row">
    <div class="col-xs-12">
        <div id="marcofooter">
            <h1>FULL DEPORTE</h1>
            <h2>EDDIE FLEISCHMAN</h2>
            <p>Todos los derechos reservados a FULLDEPORTE - EDDIE FLEISCHMAN, LIMA - PERU 2016</p>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!--[if lt IE 9]>
<script src="libs/jquery-1.8.min.js"></script>
<![endif]-->
<!--[if (gte IE 9) | (!IE)]><!-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php bloginfo('stylesheet_directory') ?>/js/libs/jquery-2.1.0.min.js"><\/script>')</script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/libs/jquery-migrate-1.2.1.min.js"></script>
<!--<![endif]-->
<script src="<?php bloginfo('stylesheet_directory') ?>/js/libs/verge.js"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/libs/easing.js"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/main.js"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/libs/easing.js"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/libs/banners.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/libs/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/libs/jcarousel.responsive.js"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/libs/jquery.bxslider.js"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/libs/imagelightbox.min.js"></script>
<script>
$('.bxslider').bxSlider({
    mode: 'fade',
    captions: false,
    controls: false,
    pager: false,
    auto: true    
});
</script>  
<script>
$( function(){

var
// ACTIVITY INDICATOR

activityIndicatorOn = function()
{
$( '<div id="imagelightbox-loading"><div></div></div>' ).appendTo( 'body' );
},
activityIndicatorOff = function()
{
$( '#imagelightbox-loading' ).remove();
},


// OVERLAY

overlayOn = function()
{
$( '<div id="imagelightbox-overlay"></div>' ).appendTo( 'body' );
},
overlayOff = function()
{
$( '#imagelightbox-overlay' ).remove();
},


// CLOSE BUTTON

closeButtonOn = function( instance )
{
$( '<button type="button" id="imagelightbox-close" title="Close"></button>' ).appendTo( 'body' ).on( 'click touchend', function(){ $( this ).remove(); instance.quitImageLightbox(); return false; });
},
closeButtonOff = function()
{
$( '#imagelightbox-close' ).remove();
},


// CAPTION

captionOn = function()
{
var description = $( 'a[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"] img' ).attr( 'alt' );
if( description.length > 0 )
$( '<div id="imagelightbox-caption">' + description + '</div>' ).appendTo( 'body' );
},
captionOff = function()
{
$( '#imagelightbox-caption' ).remove();
},


// NAVIGATION

navigationOn = function( instance, selector )
{
var images = $( selector );
if( images.length )
{
var nav = $( '<div id="imagelightbox-nav"></div>' );
for( var i = 0; i < images.length; i++ )
nav.append( '<button type="button"></button>' );

nav.appendTo( 'body' );
nav.on( 'click touchend', function(){ return false; });

var navItems = nav.find( 'button' );
navItems.on( 'click touchend', function()
{
var $this = $( this );
if( images.eq( $this.index() ).attr( 'href' ) != $( '#imagelightbox' ).attr( 'src' ) )
  instance.switchImageLightbox( $this.index() );

navItems.removeClass( 'active' );
navItems.eq( $this.index() ).addClass( 'active' );

return false;
})
.on( 'touchend', function(){ return false; });
}
},
navigationUpdate = function( selector )
{
var items = $( '#imagelightbox-nav button' );
items.removeClass( 'active' );
items.eq( $( selector ).filter( '[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"]' ).index( selector ) ).addClass( 'active' );
},
navigationOff = function()
{
$( '#imagelightbox-nav' ).remove();
},


// ARROWS

arrowsOn = function( instance, selector )
{
var $arrows = $( '<button type="button" class="imagelightbox-arrow imagelightbox-arrow-left"></button><button type="button" class="imagelightbox-arrow imagelightbox-arrow-right"></button>' );

$arrows.appendTo( 'body' );

$arrows.on( 'click touchend', function( e )
{
e.preventDefault();

var $this   = $( this ),
$target = $( selector + '[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"]' ),
index   = $target.index( selector );

if( $this.hasClass( 'imagelightbox-arrow-left' ) )
{
index = index - 1;
if( !$( selector ).eq( index ).length )
  index = $( selector ).length;
}
else
{
index = index + 1;
if( !$( selector ).eq( index ).length )
  index = 0;
}

instance.switchImageLightbox( index );
return false;
});
},
arrowsOff = function()
{
$( '.imagelightbox-arrow' ).remove();
};

$( 'a[data-imagelightbox="b1"]' ).imageLightbox({
    onStart:     function() { overlayOn(); },
    onEnd:       function() { overlayOff(); activityIndicatorOff(); },
    onLoadStart: function() { activityIndicatorOn(); },
    onLoadEnd:   function() { activityIndicatorOff(); }
});
var instanceC = $( 'a[data-imagelightbox="b"]' ).imageLightbox(
{
quitOnDocClick: false,
onStart:        function() { overlayOn(); closeButtonOn( instanceC ); },
onEnd:          function() { overlayOff(); closeButtonOff(); activityIndicatorOff(); },
onLoadStart:    function() { activityIndicatorOn(); },
onLoadEnd:      function() { activityIndicatorOff(); }
});
})
</script>  
<?php wp_footer(); ?>
</body>
</html>
  
  