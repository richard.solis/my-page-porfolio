<?php
/*
 * PostTypes Personalizados
 * -------------------------
 */

function create_post_type() {

    // banner principal
    register_post_type('Banner Principal', array(
        'labels' => array(
            'name' => __('Lista de Banners'),
            'singular_name' => __('Banner principal'),
            'all_items' => __('Ver Banners'),
            'add_new_item' => __('Agregar nuevo banner'),
            'edit_item' => __('Editar Banners'),
            'new_item' => __('Nuevo Banner'),
            'view_item' => __('Ver Banners'),
            'search_items' => __('Buscar Banners'),
            'not_found' => __('No se encontraron banners'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'banners'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        //'supports' => array('title', 'thumbnail', 'editor', 'excerpt'),
        'supports' => array('title', 'editor'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-images-alt',
    )
    );
    // futbol
    register_post_type('futbol', array(
        'labels' => array(
            'name' => __('Noticias de Futbol'),
            'singular_name' => __('Noticias de Futbol'),
            'all_items' => __('Ver Noticias de Futbol'),
            'add_new_item' => __('Agregar nueva noticia de futbol'),
            'edit_item' => __('Editar noticia de futbol'),
            'new_item' => __('Nueva noticia de futbol'),
            'view_item' => __('Ver noticias de futbol'),
            'search_items' => __('Buscar noticias de futbol'),
            'not_found' => __('No se encontraron noticias de futbol'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'futbol'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        'supports' => array('title', 'thumbnail', 'editor'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-groups',
       )
    );
    // voley
    register_post_type('voley', array(
        'labels' => array(
            'name' => __('Noticias de Voley'),
            'singular_name' => __('Noticias de Voley'),
            'all_items' => __('Ver Noticias de Voley'),
            'add_new_item' => __('Agregar nueva noticia de voley'),
            'edit_item' => __('Editar noticia de voley'),
            'new_item' => __('Nueva noticia de voley'),
            'view_item' => __('Ver noticias de voley'),
            'search_items' => __('Buscar noticias de voley'),
            'not_found' => __('No se encontraron noticias de voley'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'voley'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        'supports' => array('title', 'thumbnail', 'editor'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 7,
        'menu_icon' => 'dashicons-universal-access',
       )
    );
    // tenis
    register_post_type('tenis', array(
        'labels' => array(
            'name' => __('Noticias de Tenis'),
            'singular_name' => __('Noticias de Tenis'),
            'all_items' => __('Ver Noticias de Tenis'),
            'add_new_item' => __('Agregar nueva noticia de tenis'),
            'edit_item' => __('Editar noticia de tenis'),
            'new_item' => __('Nueva noticia de tenis'),
            'view_item' => __('Ver noticias de tenis'),
            'search_items' => __('Buscar noticias de tenis'),
            'not_found' => __('No se encontraron noticias de tenis'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'tenis'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        'supports' => array('title', 'thumbnail', 'editor'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 8,
        'menu_icon' => 'dashicons-sos',
       )
    );
    
    // atletismo
    register_post_type('atletismo', array(
        'labels' => array(
            'name' => __('Noticias de Atletismo'),
            'singular_name' => __('Noticias de Atletismo'),
            'all_items' => __('Ver Noticias de Atletismo'),
            'add_new_item' => __('Agregar nueva noticia de atletismo'),
            'edit_item' => __('Editar noticia de atletismo'),
            'new_item' => __('Nueva noticia de atletismo'),
            'view_item' => __('Ver noticias de atletismo'),
            'search_items' => __('Buscar noticias de atletismo'),
            'not_found' => __('No se encontraron noticias de atletismo'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'atletismo'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        'supports' => array('title', 'thumbnail', 'editor'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 9,
        'menu_icon' => 'dashicons-awards',
       )
    );
    // Otros
    register_post_type('otros', array(
        'labels' => array(
            'name' => __('Otras noticias'),
            'singular_name' => __('Otras noticias'),
            'all_items' => __('Ver otras noticias'),
            'add_new_item' => __('Agregar otras noticias'),
            'edit_item' => __('Editar otras noticias'),
            'new_item' => __('Nueva noticia de otros'),
            'view_item' => __('Ver noticias de otros'),
            'search_items' => __('Buscar noticias de otros'),
            'not_found' => __('No se encontraron noticias de otros'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'otros'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        'supports' => array('title', 'thumbnail', 'editor'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 9,
        'menu_icon' => 'dashicons-portfolio',
       )
    );
    // Galerias
    register_post_type('galeria', array(
        'labels' => array(
            'name' => __('Galerias'),
            'singular_name' => __('Galerias'),
            'all_items' => __('Ver otras galerias'),
            'add_new_item' => __('Agregar otras galerias'),
            'edit_item' => __('Editar otras galerias'),
            'new_item' => __('Nueva galeria'),
            'view_item' => __('Ver galerias'),
            'search_items' => __('Buscar galerias'),
            'not_found' => __('No se encontraron galerias'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'galeria'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        'supports' => array('title'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 9,
        'menu_icon' => 'dashicons-slides',
       )
    );
    // Publicidad
    register_post_type('publicidad', array(
        'labels' => array(
            'name' => __('Banners de Publicidad'),
            'singular_name' => __('Banners de Publicidad'),
            'all_items' => __('Ver otras publicaciones'),
            'add_new_item' => __('Agregar banners de publicidad'),
            'edit_item' => __('Editar banners de publicidad'),
            'new_item' => __('Nuevo banner de publicidad'),
            'view_item' => __('Ver Publicidad'),
            'search_items' => __('Buscar banners de publicidad'),
            'not_found' => __('No se encontraron banners de publicidad'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'publicidad'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        'supports' => array('title'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 9,
        'menu_icon' => 'dashicons-tickets-alt',
       )
    );
}
add_action('init', 'create_post_type');

/*
 * Taxonomias personalizada
 * ------------------------
 */

function create_taxonomy() {

    register_taxonomy('tipo', 'post', array(
        'hierarchical' => true, 'label' => 'Tipo de contenido',
        'query_var' => 'tipo',
        'rewrite' => true,
        'show_ui' => true));

}
add_action('init', 'create_taxonomy', 0);

//Custom Theme Settings
add_action('admin_menu', 'add_gcf_interface');

function add_gcf_interface() {
    add_options_page('Opciones personalizadas', 'Opciones personalizadas', '8', 'functions', 'editglobalcustomfields');
}
function editglobalcustomfields() {
    ?>
    <div class='wrap'>
    <h2>Opciones personalizadas</h2>
    <form method="post" action="options.php">
    <?php wp_nonce_field('update-options')?>

    <p><strong>Correo de contacto :</strong><br />
    <input type="text" name="input_contacto" size="45" value="<?php echo get_option('input_contacto'); ?>" /></p>

    <p><strong>Enviar datos del formulario de contáctanos a ejem(correo@email.com) :</strong><br />
    <input type="text" name="input_emailcontact" size="45" value="<?php echo get_option('input_emailcontact'); ?>" /></p>

    <p><strong>Telefono de contacto:</strong><br />
    <input type="text" name="telefono_input" size="45" value="<?php echo get_option('telefono_input'); ?>" /></p>

    <hr>
    <p><strong> Facebook NAME:</strong><br />
    <input type="text" name="fbname_input" size="45" value="<?php echo get_option('fbname_input'); ?>" /></p>
    <p><strong> Facebook Link:</strong><br />
    <input type="text" name="fblink_input" size="45" value="<?php echo get_option('fblink_input'); ?>" /></p>

    <hr>
    <p><strong> Twitter :</strong><br />
    <input type="text" name="twname_input" size="45" value="<?php echo get_option('twname_input'); ?>" /></p>

    <hr>
    <p><strong> Instagram :</strong><br />
    <input type="text" name="igname_input" size="45" value="<?php echo get_option('igname_input'); ?>" /></p>

    <hr>
    <p><strong> Youtube :</strong><br />
    <input type="text" name="ytname_input" size="45" value="<?php echo get_option('ytname_input'); ?>" /></p>

    <p><input type="submit" name="Submit" value="GUARDAR OPCIONES PERSONALIZADAS" class="button button-primary"/></p>
    <input type="hidden" name="action" value="update" />
    <input type="hidden" name="page_options" value="input_contacto,input_emailcontact,telefono_input,fbname_input,fblink_input,twname_input,igname_input,ytname_input" />

    </form>
    </div>
    <?php
}
