<?php
$BaseURL = "http://" . $_SERVER['HTTP_HOST'] . str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
?>
<!DOCTYPE html>
<html lang="<?php bloginfo('language'); ?>">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">    
    <!-- Sharing -->
    <meta property="og:title" content="FULLDEPORTE.PE">
    <meta property="og:url" content="">
    <meta property="og:image" content="<?php bloginfo('stylesheet_directory') ?>/img/share.jpg">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="Todo lo referente a Fútbol lo podrás encontrar aquí.">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    
    <meta name="description" content="Fulldeporte.pe con Eddie Fleischman. Medio deportivo digital de análisis y opinión con la mejor cobertura de los principales torneos, competencias y eventos del Perú y el mundo.">
    <meta name="keywords" content="Deportes, Análisis Deportivo, Fútbol, Voley, Atletismo, Deporte Perú" />
    
    
     <!-- Styles -->
    
    <link href="<?php bloginfo('stylesheet_directory') ?>/css/libs/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_directory') ?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_directory') ?>/fonts/fonts.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_directory') ?>/css/main.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_directory') ?>/css/libs/jquery.bxslider.css" rel="stylesheet">
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>
<body>
<div id="fb-root"></div>
<div id="contenedorp">
    <div id="trama">
        <div class="container">
            <div class="row">
                <div id="cabecerap">
                  <div class="col-md-4 col-xs-5 coo-sm-5">
                      <a id="logo" href="<?php echo $BaseURL; ?>">
                          <img src="<?php bloginfo('stylesheet_directory') ?>/img/logo.png">
                      </a>
                  </div>
                  <div class="col-md-4 col-md-offset-4 col-xs-7 col-sm-7">
                      <ul id="redes">
                       <li>
                           <a href="<?php echo $BaseURL; ?>?s=&post_type=futbol">
                               <i class="fa fa-search btnsearch" aria-hidden="true"></i>
                           </a>
                       </li>
                        <li>
                            <a href="https://www.facebook.com/FullDeporte.pe.EF" target="_blank">
                                <i class="fa fa-facebook btnfacebook" aria-hidden="true"></i>
                            </a>
                            <a href="https://twitter.com/FullDeporte_PE" target="_blank">
                                <i class="fa fa-twitter btntwitter"></i>
                            </a>
                        </li>
                       
                      </ul>
                  </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-default2 navbar-static-top">
                  <div class="container">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <!--<a class="navbar-brand" href="#">Project name</a>-->
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                      <?php wp_nav_menu(array('menu_class' => 'nav navbar-nav'));?>
                    </div>
                  </div>
                </nav>
            </div>
        </div>    