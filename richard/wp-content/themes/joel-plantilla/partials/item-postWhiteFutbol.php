<?php

	$postID = $post->ID;
	$title = get_the_title();
	$permalink = get_the_permalink();
	$thumbID = get_post_thumbnail_id($post->ID);
	$img = wp_get_attachment_image_src($thumbID, 'postTypePost_block');
	$excerpt = get_the_excerpt();
	$category = get_the_category();
	$subtitulo = get_field('the_subtitulo');




?>

<div class="col-xs-12 col-sm-4">
	<div class="placeholder">
		
		<?php
		 	$tiempoactual = current_time('timestamp');
		 	$tiempodepub = get_the_time('U');

		 	$time_difference = current_time('timestamp') - get_the_time('U');

		 		if ($time_difference < 86400) {
		 			echo '<div class="new">nuevo</div>';
		 		} else {
		 			echo '';
		 		};

		?>

		<a href="<?php echo $permalink; ?>"><img src="<?php echo $img[0];?>" class="imgfutbol"></a>
		<h4><a href="<?php echo $permalink;?>"><?php echo cutText($title, 40)?></a></h4>
		<span class="text-muted"><?php echo cutText($subtitulo, 45);?></span>
	</div>
</div>