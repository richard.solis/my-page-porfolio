<?php get_header(); ?>
<?php if( have_posts() ) : the_post(); ?>

	<div class="container blanco" id="futbol">
		<div class="row">
			<h2>Fútbol</h2>

			<?php

				$argNews = array(
					'post_type' => 'futbol',
					'posts_per_page' => 3,
					'post__not_in' => array($postID),
				);

				$loopQuery = new WP_Query($argNews);
				while ($loopQuery->have_posts()) {
					$loopQuery->the_post();
					get_template_part('partials/item', 'postWhiteFutbol');
				}
			?>
		</div>
	</div>
<?php endif; ?>
<!-- Archivo de barra lateral por defecto -->
<?php get_sidebar(); ?>
<!-- Archivo de pie global de Wordpress -->
<?php get_footer(); ?>