<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'richardW');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '-/|7.,C 5 CQmx!t4LLo5:9_6p>~U nOf.G*_oyM5[_!}?9|B+,i~kngXY<t+Ts*');
define('SECURE_AUTH_KEY', 'yh!`|]`Rz_JsOx/O%V=6XFc7SFKK{KM4R0?5;,nMf=:1*P=rz!`v_IaFPPe9vr%?');
define('LOGGED_IN_KEY', 'yXJ,op[|?.%8~XR&Au/fliL Qls9EZk:uEr4QpGw]F:t:9c5&3 7K=mivZ=]j51B');
define('NONCE_KEY', 'r^w4*Cc%T`L}[f3M8<C_r).%Q|JZ$-<scAY*Vm=P%xgxEj@R29<w[&c)rbzTe}a ');
define('AUTH_SALT', 'oIvs.=qL5<j6HYj-AU)K8+H@f{T0_L8T[iXA:9A$-F!tw,V=SW4T5.5pbSl0}|<N');
define('SECURE_AUTH_SALT', 'PTj)Md*3KY pC,l+:GAR~dV!TFFKx@*zv>#5im G:qkLgd(ay6AWk%pKU^~]:`&A');
define('LOGGED_IN_SALT', '15f9H<S6X*umHPTwqy<r*}To{UZ=Z(6[{Koo?[OD+o>c!-OVcQs8Kqnp+ 9d=1Us');
define('NONCE_SALT', 'H~h59_/#j}~zc2V5r`#My 3T%[cN+@Wv3,p8Q2J TDjJv8Q=0(%sx,xP]L`YbQ7[');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

