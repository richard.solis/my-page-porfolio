<!-- Archivo de cabecera global de Wordpres -->
<?php get_header();  ?>
<!-- Contenido de página de inicio -->
<?php if( have_posts() ) : the_post(); ?>

	<section>
		<div class="content-images">
		
		</div>
		<div class="content-info-banner">
			<div class="name-links">
				<div class="name">
					<h1>Richard Solis León</h1>
					<div class="hover-name">
						<p><</p>
						<p>></p>
					</div>
				</div>
				<div class="links">
					<ul>
						<a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-gitlab" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-github-alt" aria-hidden="true"></i></a>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section>
        <?php while ( have_posts() ) : the_post(); ?>
            <article>
                <header>
                    <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                </header>
                <?php the_excerpt(); ?>
            </article>
        <?php endwhile; ?>
        <div class="pagination">
            <span class="in-left"><?php next_posts_link('« Entradas antiguas'); ?></span>
            <span class="in-left"><?php previous_posts_link('Entradas más recientes »'); ?></span>
        </div>
    </section>
    <section>
    	<div class="container blanco" id="futbol">
			<div class="row">
				<?php

					$argNews = array(
						'post_type' => 'Acerca de Mi',
						'posts_per_page' => 1,
						'post__not_in' => array($postID),
					);

					$loopQuery = new WP_Query($argNews);
					while ($loopQuery->have_posts()) {
						$loopQuery->the_post();
						get_template_part('partials/item', 'postWhiteFutbol');
					}
				?>
			</div>
		</div>
    </section>
    <div class="Conocimientos">
	<a name="vista3"></a>
		<div class="name-section-skill">
			<h3>Habilidades</h3>
		</div>
		<div class="content-skills">
			<?php

					$argNews = array(
						'post_type' => 'Skills',
						'posts_per_page' => 6,
						'post__not_in' => array($postID),
					);

					$loopQuery = new WP_Query($argNews);
					while ($loopQuery->have_posts()) {
						$loopQuery->the_post();
						get_template_part('partials/item', 'postWhiteSkills');
					}
				?>
    	</div>
	</div>
	<section class="porfolio">
			<a name="vista4"></a>
			<div class="name-section">
				<h3>Portafolio</h3>
			</div>
			<div class="content-porfolio">
				<div class="content">
					<?php

						$argNews = array(
							'post_type' => 'PORTAFOLIO',
							'posts_per_page' => 6,
							'post__not_in' => array($postID),
						);

						$loopQuery = new WP_Query($argNews);
						while ($loopQuery->have_posts()) {
							$loopQuery->the_post();
							get_template_part('partials/item', 'postWhitePortafolio');
						}
					?>
				</div>
			</div>	
	</section>
	<section class="contactame-section">
			<a name="vista5"></a>
			<div class="name-section">
					<h3>Contáctame</h3>
			</div>
			<div class="formulario">
				<input type="text" placeholder="Nombre">
				<input type="text" placeholder="Servicio">
				<input type="text" placeholder="Correo@">
				<textarea placeholder="Comentario"></textarea>
				<button>Enviar</button>
			</div>
	</section>

	<div class="aaaaaa">
		
		<?php

					$argNews = array(
						'post_type' => 'Contact',
						'posts_per_page' => 1,
						'post__not_in' => array($postID),
					);

					$loopQuery = new WP_Query($argNews);
					while ($loopQuery->have_posts()) {
						$loopQuery->the_post();
						get_template_part('partials/item', 'postWhiteContact');
					}
				?>

	</div>

<?php endif; ?>
fgcfgfc
<!-- Archivo de barra lateral por defecto -->
<?php get_sidebar(); ?>
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>