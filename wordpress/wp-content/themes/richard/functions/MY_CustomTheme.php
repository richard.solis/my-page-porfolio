<?php
/*
 * PostTypes Personalizados
 * -------------------------
 */

function create_post_type() {

    // banner principal
    register_post_type('Banner Principal', array(
        'labels' => array(
            'name' => __('Lista de Banners'),
            'singular_name' => __('Banner principal'),
            'all_items' => __('Ver Banners'),
            'add_new_item' => __('Agregar nuevo banner'),
            'edit_item' => __('Editar Banners'),
            'new_item' => __('Nuevo Banner'),
            'view_item' => __('Ver Banners'),
            'search_items' => __('Buscar Banners'),
            'not_found' => __('No se encontraron banners'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'banners'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        //'supports' => array('title', 'thumbnail', 'editor', 'excerpt'),
        'supports' => array('title', 'editor'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-images-alt',
    )
    );
    // futbol
    register_post_type('Acerca de Mi', array(
        'labels' => array(
            'name' => __('Acerca de Mi'),
            'singular_name' => __('Noticias de Futbol'),
            'all_items' => __('Ver Noticias de Futbol'),
            'add_new_item' => __('Agregar nueva noticia de futbol'),
            'edit_item' => __('Editar noticia de futbol'),
            'new_item' => __('Nueva noticia de futbol'),
            'view_item' => __('Ver noticias de futbol'),
            'search_items' => __('Buscar noticias de futbol'),
            'not_found' => __('No se encontraron noticias de futbol'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'futbol'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        'supports' => array('title', 'thumbnail', 'editor'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-groups',
       )
    );

    // skills
    register_post_type('skills', array(
        'labels' => array(
            'name' => __('skills'),
            'singular_name' => __('Noticias de Futbol'),
            'all_items' => __('Ver Noticias de Futbol'),
            'add_new_item' => __('Agregar nueva noticia de futbol'),
            'edit_item' => __('Editar noticia de futbol'),
            'new_item' => __('Nueva noticia de futbol'),
            'view_item' => __('Ver noticias de futbol'),
            'search_items' => __('Buscar noticias de futbol'),
            'not_found' => __('No se encontraron noticias de futbol'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'futbol'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        'supports' => array('title', 'thumbnail', 'editor'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-groups',
       )
    );

    // PORTAFOLIO
    register_post_type('PORTAFOLIO', array(
        'labels' => array(
            'name' => __('PORTAFOLIO'),
            'singular_name' => __('Noticias de Futbol'),
            'all_items' => __('Ver Noticias de Futbol'),
            'add_new_item' => __('Agregar nueva noticia de futbol'),
            'edit_item' => __('Editar noticia de futbol'),
            'new_item' => __('Nueva noticia de futbol'),
            'view_item' => __('Ver noticias de futbol'),
            'search_items' => __('Buscar noticias de futbol'),
            'not_found' => __('No se encontraron noticias de futbol'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'futbol'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        'supports' => array('title', 'thumbnail', 'editor'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-groups',
       )
    );

    // Contact
    register_post_type('Contact', array(
        'labels' => array(
            'name' => __('Contact'),
            'singular_name' => __('Noticias de Futbol'),
            'all_items' => __('Ver Noticias de Futbol'),
            'add_new_item' => __('Agregar nueva noticia de futbol'),
            'edit_item' => __('Editar noticia de futbol'),
            'new_item' => __('Nueva noticia de futbol'),
            'view_item' => __('Ver noticias de futbol'),
            'search_items' => __('Buscar noticias de futbol'),
            'not_found' => __('No se encontraron noticias de futbol'),
            'parent_item_colon' => '',
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'futbol'),
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'hierarchical' => false,
        'query_var' => true,
        'supports' => array('title', 'thumbnail', 'editor'),
        'show_in_menu' => true,
        'show_ui' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-groups',
       )
    );
}
add_action('init', 'create_post_type');